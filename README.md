# 2^11 = 2048

Copyright (C) 2024 2to11 contributors

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>

Please see the `LICENSE` file for license information of this project. Additional development tools are not necessarily under the same license.

## What it is

This is a complete recreation of 2048 by Gabriele Cirulli in HTML, CSS, and JavaScript (ES2020). Instead of the original game's HTML elements and CSS animations, we use the JavaScript Canvas API. The game is described in more detail in `index.html`.

In short:
- The look of the game is near-identical to the original, using the same (painfully reverse-engineered) color scheme and tile sizes by default. Tile glow has not been implemented yet.
- The game saves progress and high score.
- You can customize the board size from 1x1 to 10x10. No, 1x1 is not very fun.
- We use a service worker and an app manifest so that supported Web browsers can "install" the game as a Progressive Web App which works offline.
- Several accessibility features have been implemented: high contrast, dark color scheme, and proper keyboard focus.
- The game is more responsive to touch than the original.

## Development information

In addition to this README file, the source code is quite verbosely commented. There is a `TODO` file which contains a few things I want to do in the future. More TODOs are in the source code.

The source HTML, JavaScript, and CSS is in `src/`, as well as the app manifest. The main logic of the game is in `src/game.js`. Icons for the game are in `src/img/`. Package information and dependencies are in `package.json`.

Because we use quite recent JavaScript features, the browser support for this game is only about 90%. This is fine by me.

The StandardJS style guide is used with ESLint. Before contributing a patch, please check your code with it. Use the `npm install` command to initialize the ESLint dependency, and use `npx eslint src/` to check for errors in the code. ESLint is also integrated in many editors, see https://eslint.org/docs/user-guide/integrations for details. ESLint configuration is in `.eslintrc.js`.

## Deployment information

The `src/` directory contains all that is needed to serve the game and simply needs to be dropped in your HTTP server's directory. The game is independent of the URL it is served from, so you can serve it from any subdirectory under any name.

To update the game, just replace the older source directory with the new.

/* Copyright (C) 2024 2to11 contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>
 */

/**
 * Various utility functions and declarations.
 **/

/// An enum representing possible directions of movement.
const Directions = {
  Left: 0,
  Right: 1,
  Up: 2,
  Down: 3
}
Object.freeze(Directions)

// First, second, and third degree Bézier functions.
// These are correct but one-dimensional, you can only pass one coordinate of
// your points at a time.

/// Linear Bézier function. Equivalent to linear interpolation.
/// Returns a function `f(t)` which goes from `p0` to `p1` as `t` goes from 0 to 1.
function linearBezier (p0, p1) {
  return t => t * p1 + (1 - t) * p0
}
/// Quadratic Bézier function.
/// Returns a function `f(t)` which goes from `p0` to `p2` as `t` goes from 0 to 1.
function quadraticBezier (p0, p1, p2) {
  return t => linearBezier(
    linearBezier(p0, p1)(t),
    linearBezier(p1, p2)(t)
  )(t)
}
/// Cubic Bézier function.
/// Returns a function `f(t)` which goes from `p0` to `p3` as `t` goes from 0 to 1.
function cubicBezier (p0, p1, p2, p3) {
  return t => linearBezier(
    quadraticBezier(p0, p1, p2)(t),
    quadraticBezier(p1, p2, p3)(t)
  )(t)
}

/// Cubic Bézier as defined in CSS, to make CSS animations in JavaScript.
/// CSS defines Bézier functions in an annoying and expensive way, so
/// we have to use a pre-computed table.
function CSSCubicBezier (x0, y0, x1, y1) {
  const P0 = [0, 0]
  const P1 = [x0, y0]
  const P2 = [x1, y1]
  const P3 = [1, 1]

  // Number of cache entries. 250 is plenty smooth.
  const cacheEntries = 250
  const precision = 1 / cacheEntries

  // Now we've got the coefficients of a cubic equation for the x and y
  // coordinates. CSS cubic Bézier gives the y coordinate for a given x,
  // instead of giving a x,y pair for a given t. I don't want to solve a
  // cubic equation, so instead let's do a binary search.
  function findY (x) {
    let min = 0
    let max = 1
    let t

    let estX

    do {
      t = (min + max) / 2
      estX = cubicBezier(P0[0], P1[0], P2[0], P3[0])(t)

      if (x > estX) {
        min = t
      } else {
        max = t
      }
    } while (Math.abs(estX - x) > precision)

    return cubicBezier(P0[1], P1[1], P2[1], P3[1])(t)
  }

  const cache = []
  for (let i = 0; i <= cacheEntries; i++) {
    cache.push(findY(i / cacheEntries))
  }

  // Return the closest entry from cache.
  return x => cache[Math.round(x * cacheEntries)]
}

/// Choose a random element in a collection.
function choose (collection) {
  return collection[Math.floor(Math.random() * collection.length)]
}

/// Takes an angle in radians and returns the closest corresponding `Direction`,
/// either up, down, left, or right.
function angleToDirection (angle) {
  // Normalize the angle to be between 0 and 2*pi.

  while (angle < 0) {
    angle += 2 * Math.PI
  }

  while (angle > 2 * Math.PI) {
    angle -= 2 * Math.PI
  }

  let dir
  if (angle > 0.25 * Math.PI && angle <= 0.75 * Math.PI) {
    dir = Directions.Up
  } else if (angle > 0.75 * Math.PI && angle <= 1.25 * Math.PI) {
    dir = Directions.Right
  } else if (angle > 1.25 * Math.PI && angle <= 1.75 * Math.PI) {
    dir = Directions.Down
  } else {
    dir = Directions.Left
  }

  return dir
}

/// Build an HTML element with a `tagName` and a dictionnary of `properties`.
/// Calls `callback()` with the resulting element and returns the element.
function buildElement (tagName, properties, callback) {
  const element = document.createElement(tagName)

  for (const [property, value] of Object.entries(properties)) {
    element[property] = value
  }

  if (callback !== undefined) {
    callback(element)
  }

  return element
}

/// Make and return an element containing radio buttons
/// defined by "name: value" pairs in `options`
function makeRadio (radioName, options) {
  return buildElement('div', {}, radio => {
    for (const [optionName, optionLabel] of Object.entries(options)) {
      radio.appendChild(buildElement('label', {}, radioLabel => {
        radioLabel.appendChild(buildElement('input', {
          type: 'radio',
          name: radioName,
          value: optionName
        }))

        radioLabel.appendChild(document.createTextNode(optionLabel))
      }))
    }
  })
}

/// Make and return a score display container with a `label`
/// and append it to `parent`.
function makeScoreContainer (parent, label) {
  const scoreDisplayContainer = buildElement('section', {
    className: 'score'
  })

  scoreDisplayContainer.appendChild(buildElement('span', {
    className: 'score-label',
    textContent: label
  }))

  const scoreDisplay = buildElement('span', {
    className: 'score-count'
  })
  scoreDisplayContainer.appendChild(scoreDisplay)

  parent.appendChild(scoreDisplayContainer)

  return scoreDisplay
}

export default {
  Directions,
  linearBezier,
  CSSCubicBezier,
  choose,
  angleToDirection,
  buildElement,
  makeRadio,
  makeScoreContainer
}

/* Copyright (C) 2024 2to11 contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>
 */

/**
 * Various settings to control how the game behaves.
 **/

import util from './util.js'

export const settings = {
  name: '2to11',
  version: '0.1.11',

  defaultBoardDimensions: [4, 4],

  // In milliseconds
  animationSpeeds: {
    popUp: 200,
    appear: 200,
    move: 100
  },

  powerFunction: value => Math.pow(2, value),

  // Based on the original SCSS: ratio between spacing length and and tile size
  spacingProportion: 15 / 107,
  tileBorderRadius: 3 / 107,

  // These are the transition functions used by the original game.
  timingFunctions: {
    easeInOut: util.CSSCubicBezier(0.42, 0, 0.58, 1),
    ease: util.CSSCubicBezier(0.25, 0.1, 0.25, 1)
  },
  scalingFunctions: {
    still: t => 1
  }
}

settings.timingFunctions.moving = settings.timingFunctions.easeInOut

settings.scalingFunctions.appear = settings.timingFunctions.ease
settings.scalingFunctions.pop = t => {
  if (t < 0.5) {
    return settings.timingFunctions.ease(2 * t) * 1.2
  } else {
    return 1.2 - 0.2 * settings.timingFunctions.ease(2 * (t - 0.5))
  }
}

/* Copyright (C) 2024 2to11 contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>
 */

/**
 * Drawing logic. This is collection of routines for drawing the game on screen
 * using the JavaScript Canvas API. This is called from the `game.js` module.
 **/

import { settings } from './settings.js'
import colors from './colors.js'

export default {
  drawFrame,
  drawTile,
  setup
}

const fontList = '"Clear Sans", "Helvetica Neue", Arial, sans-serif'

let gameObject

/// Get a reference to the game object, for access to variables external
/// to this module.
function setup (_gameObject) {
  gameObject = _gameObject
}

/// Convert a tile's position in [0..1, 0..1] to pixel coordinates,
/// rounded to the nearest pixels.
function getTilePosition (x, y) {
  return [
    Math.round(
      x * (gameObject.variables.sizes.tileSize + gameObject.variables.sizes.spacingLength) +
        gameObject.variables.sizes.spacingLength
    ),
    Math.round(
      y * (gameObject.variables.sizes.tileSize + gameObject.variables.sizes.spacingLength) +
        gameObject.variables.sizes.spacingLength
    )
  ]
}

// TODO: Here lie the remains of my attempt at making tile glow.
/* function drawShadow (shadow) {
  const x = gameObject.variables.sizes.tileSize * -shadow.spread
  const y = gameObject.variables.sizes.tileSize * -shadow.spread

  const width = gameObject.variables.sizes.tileSize * (2 * shadow.spread + 1)
  const height = width
  const size = shadow.size * gameObject.variables.sizes.tileSize

  gameObject.context.fillStyle = shadow.color

  gameObject.context.beginPath()
  drawRoundedRectangle(
    x, y,
    width, height,
    settings.tileBorderRadius * width
  )
  gameObject.context.filter = `drop-shadow(0px 0px ${size}px ${shadow.color})`
  gameObject.context.fill()
  gameObject.context.filter = 'none'
} */

/// Draw a rounded rectangle at (`x`, `y`) with dimensions `width` and `height`
/// and a `cornerRadius`, similar to a `CanvasRenderingContext2D.rect()`.
function drawRoundedRectangle (x, y, width, height, cornerRadius) {
  function arcRelative (dx, dy, angleStart, angleEnd) {
    gameObject.context.arc(
      x + dx, y + dy, cornerRadius,
      angleStart, angleEnd
    )
  }

  gameObject.context.moveTo(x, y + cornerRadius)
  arcRelative(cornerRadius, cornerRadius, 1.0 * Math.PI, 1.5 * Math.PI)
  arcRelative(width - cornerRadius, cornerRadius, 1.5 * Math.PI, 2.0 * Math.PI)
  arcRelative(width - cornerRadius, height - cornerRadius, 0.0 * Math.PI, 0.5 * Math.PI)
  arcRelative(cornerRadius, height - cornerRadius, 0.5 * Math.PI, 1.0 * Math.PI)
  gameObject.context.lineTo(x, y + cornerRadius)
}

/// Draw a tile at (`x`, `y`). `scale` is used for animatating a tile appearing
/// or merging. `value` is the tile's value in [1, 2, 3, ...].
function drawTile (x, y, scale, value) {
  const tileColors = colors.getTileColors(value)
  // Convert a value to its on-screen value.
  // In 2048, 1 => 2^1, 2 => 2^2, etc.
  const text = settings.powerFunction(value).toString()
  const tilePosition = getTilePosition(x, y)

  // Make the drawing context temporarily relative to tile position,
  // accounting for tile scale.
  gameObject.context.save()
  gameObject.context.translate(tilePosition[0], tilePosition[1])
  gameObject.context.translate(
    Math.round(0.5 * (1 - scale) * gameObject.variables.sizes.tileSize),
    Math.round(0.5 * (1 - scale) * gameObject.variables.sizes.tileSize)
  )
  gameObject.context.scale(scale, scale)

  // TODO: tile glow.
  /* for (const shadow of tileColors.shadows) {
    drawShadow(shadow)
  } */

  gameObject.context.fillStyle = tileColors.backgroundColor
  gameObject.context.beginPath()
  drawRoundedRectangle(
    0, 0,
    Math.floor(gameObject.variables.sizes.tileSize), Math.floor(gameObject.variables.sizes.tileSize),
    settings.tileBorderRadius * gameObject.variables.sizes.tileSize
  )
  gameObject.context.fill()

  let fontSize = 30 / 107
  if (value < 12) {
    // Original game's tile size is ceil(106.25px) for a default
    // [55px, 45px, 35px] font size depending on the number of digits
    // [1-2, 3, 4+]
    const textSizeProportion = [
      55 / 107, 55 / 107,
      45 / 107,
      35 / 107
    ][text.length - 1]
    fontSize = textSizeProportion
  }
  fontSize *= gameObject.variables.sizes.tileSize

  // I don't want to buy the original 2048 font. Maybe the user has it installed?
  gameObject.context.font = `bold ${fontSize}px "Clear Sans", "Helvetica Neue", Arial, sans-serif`
  // Center text horizontally. Vertical centering is done by shifting coordinates.
  gameObject.context.textAlign = 'center'
  gameObject.context.textBaseline = 'alphabetic'

  gameObject.context.fillStyle = tileColors.color
  gameObject.context.fillText(
    text,
    gameObject.variables.sizes.tileSize / 2,
    // This vertical shifting is a bit hacky, the 0.45 coefficient
    // was found by trial-and-error.
    0.45 * gameObject.variables.sizes.tileSize + fontSize / 2,
    gameObject.variables.sizes.tileSize
  )

  // Restore the drawing context to absolute coordinates.
  gameObject.context.restore()
}

/// Draw a full frame: background grid and tiles.
function drawFrame () {
  const gridColors = colors.getGridColors()

  // Draw background
  gameObject.context.fillStyle = gridColors.background
  gameObject.context.beginPath()
  gameObject.context.rect(
    0, 0,
    gameObject.variables.sizes.boardSize[0], gameObject.variables.sizes.boardSize[1]
  )
  gameObject.context.fill()

  // Draw background grid
  for (let y = 0; y < gameObject.variables.boardDimensions[1]; y++) {
    for (let x = 0; x < gameObject.variables.boardDimensions[0]; x++) {
      const tilePosition = getTilePosition(x, y)

      gameObject.context.fillStyle = gridColors.tileBackground

      gameObject.context.beginPath()
      drawRoundedRectangle(
        tilePosition[0], tilePosition[1],
        Math.floor(gameObject.variables.sizes.tileSize),
        Math.floor(gameObject.variables.sizes.tileSize),
        settings.tileBorderRadius * gameObject.variables.sizes.tileSize
      )
      gameObject.context.fill()
    }
  }

  // Draw tiles
  for (const tile of gameObject.variables.tiles) {
    tile.draw()
  }

  const undoIsShown = gameObject.elements.undoButton.classList.contains('show')
  if (undoIsShown !== gameObject.variables.gameOver) {
    gameObject.elements.undoButton.classList.toggle('show')
  }

  if (gameObject.variables.gameOver) {
    gameObject.context.fillStyle = colors.RGBAToCSS(colors.colorSchemeTransformRGBA([0.93, 0.89, 0.85, 0.73]))
    gameObject.context.fillRect(0, 0, gameObject.canvas.width, gameObject.canvas.height)

    gameObject.context.textAlign = 'center'
    gameObject.context.textBaseline = 'middle'
    gameObject.context.font = `bold ${Math.min(gameObject.canvas.width * 0.1, 0.6 * gameObject.variables.sizes.tileSize)}px ${fontList}`

    gameObject.context.fillStyle = colors.RGBToCSS(colors.colorSchemeTransformRGB([0.47, 0.43, 0.40]))
    gameObject.context.fillText(
      'Game over!',
      gameObject.canvas.width / 2,
      gameObject.canvas.height / 2
    )
  } else if (!gameObject.focused) {
    // Draw a message if the game is unfocused. This is because we only capture
    // keyboard events on the canvas itself for accessibility.
    gameObject.context.fillStyle = 'rgba(0, 0, 0, 0.2)'
    gameObject.context.fillRect(0, 0, gameObject.canvas.width, gameObject.canvas.height)

    gameObject.context.font = `${Math.min(gameObject.canvas.width * 0.05, 0.25 * gameObject.variables.sizes.tileSize)}px ${fontList}`
    gameObject.context.textAlign = 'center'
    gameObject.context.textBaseline = 'middle'

    gameObject.context.fillStyle = 'white'
    gameObject.context.fillText(
      'Game is unfocused. Click to play',
      gameObject.canvas.width / 2,
      gameObject.canvas.height / 2
    )
  }
}

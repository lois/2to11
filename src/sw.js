/* Copyright (C) 2024 2to11 contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>
 */

/**
 * Service worker logic. From here we can intercept requests to cache them
 * long-term so that the game works offline. We also pre-cache essential
 * resources immediatly.
 **/

/// The cache name for this version of the service worker.
/// In the format "2to11-x.y.z".
const cacheVersion = '2to11-0.1.11'

/// Files that should be always be cached immediately, because they are
/// essential to the game.
const precache = [
  'index.html',
  'style.css',
  'game.js',
  'colors.js',
  'drawing.js',
  'settings.js',
  'util.js',
  'img/icon_16.png',
  'app.webmanifest'
]

/// Returns whether a given (request, response) pair should be cached.
function shouldCache (request, response) {
  // If the request failed, do not cache.
  if (!response.ok) {
    console.error('Fetch for', request.url, 'failed')
    return false
  }

  // Get MIME type for Content-Type response header.
  let contentType = response.headers.get('Content-Type')
  if (contentType.includes(';')) {
    contentType = contentType.split(';')[0]
  }
  const mimeType = contentType.trim()

  // Cache all images.
  if (mimeType.startsWith('image/')) return true

  // Cache all HTML, CSS, and JS.
  if ([
    'text/javascript', 'application/javascript',
    'text/html',
    'text/css',
    'application/json'
  ].includes(mimeType)) return true

  console.warn('Resource', request.url, 'is not considered cachable')

  return false
}

// Install event: the service worker is installing and we should take this time
// to prepare the network cache.
self.addEventListener('install', event => {
  // Do not wait for clients with older versions of the service worker to
  // close. Since this service worker is just a cache, there is no need to be
  // careful about which clients get which service worker, so this is safe.
  // And it helps new game versions to load as soon as they are available.
  self.skipWaiting()

  // Delay install before all precached resources are loaded.
  event.waitUntil(
    caches.open(cacheVersion).then(cache => {
      return cache.addAll(precache)
    })
  )
})

// Activate event: the service worker is about to start serving network
// requests.
self.addEventListener('activate', event => {
  // Delay activation until all clients in scope are controlled by
  // this version of the service worker (in case an older version is running)
  event.waitUntil(self.clients.claim())

  // Delay activation until every cache in scope (except for the current one)
  // is deleted. We don't want to keep older cache versions.
  event.waitUntil(caches.keys().then(cacheList => {
    return Promise.all(
      cacheList
        .filter(cacheName => cacheVersion !== cacheName)
        .map(cacheName => caches.delete(cacheName))
    )
  }))
})

// Fetch event: This is run whenever a request is made by the app.
self.addEventListener('fetch', event => {
  console.info('Fetch event for', event.request.url)

  const response = caches.match(event.request).then(response => {
    // If the response is already cached, return it.
    if (response !== undefined) {
      console.info('Cache match for', event.request.url)
      return response
    } else {
      console.info('Cache miss for', event.request.url)

      // Otherwise we have to fetch it to return it.
      return fetch(event.request).then(response => {
        if (shouldCache(event.request, response)) {
          // Store a copy of the resource in cache.
          const responseClone = response.clone()
          caches.open(cacheVersion).then(cache => {
            cache.put(event.request, responseClone)
          })
        }

        return response
      }).catch(error => {
        console.error('Error fetching resource:', event.request)

        throw error
      })
    }
  })

  if (response !== null) {
    event.respondWith(response)
  }
})

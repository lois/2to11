/* Copyright (C) 2024 2to11 contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>
 */

/**
 * Color handling. This is where we make the color scheme according
 * to user settings.
 **/

export default {
  makeColors,
  getTileColors,
  getGridColors,
  RGBToCSS,
  RGBAToCSS,
  colorSchemeTransformRGB,
  colorSchemeTransformRGBA,
  hexToRGB
}

/// Current computed colors.
const colors = {
  grid: {
    background: null,
    tileBackground: null,
    textColor: null,
    brightTextColor: null
  },
  tile: {
    color: null,
    goldColor: null,
    goldGlowColor: null,
    colors: null,
    defaultColor: {
      color: null,
      backgroundColor: null,
      shadows: []
    }
  }
}

/// Settings which control the color scheme.
const settings = {
  darkMode: false,
  moreContrast: false
}

/// Converts a hexadecimal color in the format #RRGGBB to an array of red,
/// green, and blue values between 0 and 1.
function hexToRGB (hexString) {
  const r = parseInt(hexString.slice(1, 3), 16) / 255
  const g = parseInt(hexString.slice(3, 5), 16) / 255
  const b = parseInt(hexString.slice(5, 7), 16) / 255

  return [r, g, b]
}

/// Converts a RGB color in the form [0..1, 0..1, 0..1] into a CSS color in
/// the form "rgb(0..255, 0..255, 0..255)".
function RGBToCSS (color) {
  const r = 255.0 * color[0]
  const g = 255.0 * color[1]
  const b = 255.0 * color[2]

  return `rgb(${r},${g},${b})`
}

/// Converts a RGBA color in the form [0..1, 0..1, 0..1, 0..1] into a CSS color
/// in the form "rgba(0..255, 0..255, 0..255, 0..1)".
function RGBAToCSS (color) {
  const r = 255.0 * color[0]
  const g = 255.0 * color[1]
  const b = 255.0 * color[2]
  const a = color[3]

  return `rgba(${r},${g},${b},${a})`
}

/// Mix two RGB colors in the form [0..1, 0..1, 0..1] according to a `weight`
/// between 0 and 1. A weight of 0 means only `color2` and 1 means only `color1`
///
/// This is adapted from dart-sass, to reproduce 2048's behavior.
/// Alpha handling is removed because we don't need it, so the function
/// just becomes a weighted average of every RGB component.
/// (which isn't perceptually correct for a "mix" function but hey)
/// Also we don't "fuzzy round" so the colors might extremely slightly
/// different but I genuinely don't care at this point I have done way too
/// much work when I could just have copied the compiled CSS but fuck me right?
///
/// I'm okay.
function mixColors (color1, color2, weight) {
  return [
    color1[0] * weight + color2[0] * (1 - weight),
    color1[1] * weight + color2[1] * (1 - weight),
    color1[2] * weight + color2[2] * (1 - weight)
  ]
}

/// Invert a RGB color in the form [0..1, 0..1, 0..1]
function invertColorRGB (color) {
  return [
    1.0 - color[0],
    1.0 - color[1],
    1.0 - color[2]
  ]
}

/// Invert a RGBA color in the form [0..1, 0..1, 0..1, 0..1], preserving the
/// alpha value as is.
function invertColorRGBA (color) {
  return [
    1.0 - color[0],
    1.0 - color[1],
    1.0 - color[2],
    color[3]
  ]
}

/// Inverts a RGB color in the form [0..1, 0..1, 0..1] only if
/// `settings.darkMode` is true.
function colorSchemeTransformRGB (color) {
  if (settings.darkMode) {
    return invertColorRGB(color)
  } else {
    return color
  }
}

/// Inverts a RGBA color in the form [0..1, 0..1, 0..1, 0..1] only if
/// `settings.darkMode` is true.
function colorSchemeTransformRGBA (color) {
  if (settings.darkMode) {
    return invertColorRGBA(color)
  } else {
    return color
  }
}

/// Update the `colors` global variable according to the provided settings.
///
/// This is adapted from Gabriele Cirulli's 2048 on GitHub:
/// https://github.com/gabrielecirulli/2048/blob/fc1ef4fe5a5fcccea7590f3e4c187c75980b353f/style/main.scss
/// The general structure is the same but some names have been changed, and
/// some tidying up has been done to make things more concise and idiomatic.
function makeColors (darkMode, moreContrast) {
  settings.darkMode = darkMode
  settings.moreContrast = moreContrast

  colors.grid.background = RGBToCSS(colorSchemeTransformRGB(hexToRGB('#bbada0')))
  colors.grid.tileBackground = RGBAToCSS(colorSchemeTransformRGBA([238, 228, 218, 0.35]))

  colors.grid.textColor = colorSchemeTransformRGB(hexToRGB('#776e65'))
  colors.grid.brightTextColor = colorSchemeTransformRGB(hexToRGB('#f9f6f2'))

  colors.tile.color = colorSchemeTransformRGB(hexToRGB('#eee4da'))
  colors.tile.goldColor = colorSchemeTransformRGB(hexToRGB('#edc22e'))
  colors.tile.goldGlowColor = colorSchemeTransformRGB(hexToRGB('#f3d774'))

  colors.tile.defaultColor.color = RGBToCSS(colorSchemeTransformRGB(colors.grid.brightTextColor))
  colors.tile.defaultColor.backgroundColor = RGBToCSS(mixColors(
    colorSchemeTransformRGB(hexToRGB('#333333')),
    colors.tile.goldColor,
    0.95
  ))

  // Colors for all 11 states, false = no special color
  const specialColors = [
    [false, false], // 2
    [false, false], // 4
    [colorSchemeTransformRGB(hexToRGB('#f78e48')), true], // 8
    [colorSchemeTransformRGB(hexToRGB('#fc5e2e')), true], // 16
    [colorSchemeTransformRGB(hexToRGB('#ff3333')), true], // 32
    [colorSchemeTransformRGB(hexToRGB('#ff0000')), true], // 64
    [false, true], // 128
    [false, true], // 256
    [false, true], // 512
    [false, true], // 1024
    [false, true] // 2048
  ]

  const newTileColors = []
  const limit = 11

  for (let exponent = 1; exponent <= limit; exponent++) {
    const goldProportion = (exponent - 1) / (limit - 1)
    let backgroundColor = mixColors(colors.tile.goldColor, colors.tile.color, goldProportion)

    const [specialBackground, isBrightColor] = specialColors[exponent - 1]

    const glowOpacity = Math.max(exponent - 4, 0) / (limit - 4)
    const shadows = []

    if (specialBackground) {
      backgroundColor = mixColors(specialBackground, backgroundColor, 0.55)
    } else {
      shadows.push({
        size: 30 / 107,
        spread: 10 / 107,
        color: RGBAToCSS(colors.tile.goldGlowColor.concat(glowOpacity / 1.8))
      })
      /* shadows.push({
            size: 0,
            spread: 1,
            color: RGBAToCSS([1.0, 1.0, 1.0, glowOpacity / 3]),
            inset: true
          }) */
    }

    const color = isBrightColor ? colors.grid.brightTextColor : colors.grid.textColor

    newTileColors.push({
      backgroundColor: RGBToCSS(backgroundColor),
      color: RGBToCSS(color),
      shadows
    })
  }

  colors.tile.colors = newTileColors
}

/// Get the colors of a tile of value `value` according to current settings.
function getTileColors (value) {
  if (settings.moreContrast) {
    return {
      color: 'black',
      backgroundColor: 'white',
      shadows: []
    }
  }

  if (colors.tile.colors[value - 1] === undefined) {
    return colors.tile.defaultColor
  }

  return colors.tile.colors[value - 1]
}

/// Get the colors of the grid behind the tiles, according to current settings.
function getGridColors () {
  if (settings.moreContrast) {
    return {
      background: 'black',
      tileBackground: '#999999',
      textColor: 'white',
      brightTextColor: 'white'
    }
  } else {
    return colors.grid
  }
}

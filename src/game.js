/* Copyright (C) 2024 2to11 contributors
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>
 */

/**
 * This file contains the game's core tiling logic and
 * interacts with the DOM. It defers other functionality
 * to these files:
 *
 * Color handling, including dark mode, is in colors.js
 * Drawing is handled in drawing.js
 * Customizable settings are in settings.js
 * The service worker handling offline mode is in sw.js
 * Various utilities are in util.js
 **/

import drawing from './drawing.js'
import colors from './colors.js'
import { settings } from './settings.js'
import util from './util.js'

// If service workers are supported in this browser, we load sw.js
// for offline mode support.
if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('sw.js').then(registration => {
      if (registration.installing) {
        console.info('Service worker installing.')
      } else if (registration.waiting) {
        console.info('Service worker installed.')
      } else if (registration.active) {
        console.info('Service worker active.')

        registration.update().then(() => {
          console.info('Successfully updated service worker.')
        }).catch(updateError => {
          console.info('Could not update service worker:', updateError)
        })
      }
    }).catch(registrationError => {
      console.warn('Service worker registration failed:', registrationError)
    })
  })
}

/// This variable holds all DOM elements that we need to interact with.
const elements = {}

/// This variable contains various dynamic information about the game's state.
const variables = {
  sizes: {
    /// This is the board's size in pixels, width and height.
    boardSize: [null, null],

    /// This is the size of a tile.
    tileSize: null,
    /// This is the amount of space between tiles as well as between the edges
    /// of the board and the tiles.
    spacingLength: null
  },

  /// Represents whether the game is over because the player cannot move anymore
  gameOver: false,

  /// This is the board's dimensions, in columns then rows.
  boardDimensions: settings.defaultBoardDimensions,
  /// This is a list of the board's tiles at every index on the board.
  /// Indexes start on the top left and go left to right, then top to bottom.
  /// Some tiles that are animating might not appear in the place that they are
  /// marked here, but as far as the game's logic is concerned, tiles travel
  /// instantly to their destination.
  /// A tile's value goes from 1,2,3 to infinity.
  /// 1 represents 2^1 = 2, 2 represents 2^2 = 4, etc.
  board: null,
  /// This is an unordered list of the board's tiles, whether they are static,
  /// moving, appearing or disappearing.
  tiles: null,

  /// This is a history of previous `board` states in the current game, for
  /// undoing moves.
  history: [],

  /// Right now we keep track of the current and best all time score in a
  /// single place. TODO: make this instanciated for every profile when we
  /// have profiles.
  scores: {
    current: 0,
    best: 0
  },

  /// This variable is set to true whenever tiles are still animating on
  /// screen. If no animations are running, this is set to false and will
  /// prevent the useless redrawing of the exact same screen, which is good
  /// for performance and energy efficiency.
  needRedraw: true,
  /// The last time that a frame was drawn. This is used to tell how much time
  /// has passed since the last frame, allowing logic to run at the same speed
  /// for any frame rate.
  lastFrameTime: null
}

class Game {
  /// This initializes everything we can without interacting with the document,
  /// because this can run before it is fully loaded. Starting the game and
  /// adding the game's elements to the DOM is in `this.load()`.
  constructor () {
    // Local references to the global `variables` and `elements`.
    this.variables = variables
    this.elements = elements

    this.userSettings = {
      colorScheme: 'auto',
      showUndoButton: false
    }

    this.buildDOM()
    this.listen()

    this.reset()
    this.loadSave()

    this.makeColors()
    this.updateUndoButton()

    // Provide a reference to this instance for the drawing module so that it
    // has a canvas and context to write to.
    drawing.setup(this)
  }

  /// Build the game's elements: buttons, information, canvas, etc.
  buildDOM () {
    // This is the main element which contains everything else. It will be
    // appended to the <main> element in index.html.
    this.elements.game = util.buildElement('section', {
      className: 'game'
    })

    // This is the top bar which currently contains buttons and score display.
    this.elements.game.appendChild(util.buildElement('div', {
      className: 'top-bar'
    }, topBar => {
      // This is the new game button. When clicked it calls `this.newGame()`
      topBar.appendChild(util.buildElement('button', {
        textContent: 'New game'
      }, newGameButton => {
        newGameButton.addEventListener('click', this.newGame.bind(this))
      }))

      // This is the settings button. When clicked it toggles the hidden flag
      // on the settings section, which is defined later in this function.
      topBar.appendChild(util.buildElement('button', {
        textContent: 'Settings'
      }, settingsButton => {
        settingsButton.addEventListener('click', () => {
          this.elements.settingsSection.classList.toggle('hidden')
        })
      }))

      // This is the score container, which contains the best and current scores.
      topBar.appendChild(util.buildElement('div', {
        className: 'score-container'
      }, scoreContainer => {
        this.elements.bestScoreDisplay = util.makeScoreContainer(scoreContainer, 'Best')
        this.elements.scoreDisplay = util.makeScoreContainer(scoreContainer, 'Score')
      }))
    }))

    // This is the settings sections, which contains some settings the user can set.
    this.elements.settingsSection = util.buildElement('div', {
      // It starts hidden but can be shown via a top bar button.
      className: 'settings hidden'
    }, settingsSection => {
      settingsSection.appendChild(util.buildElement('h2', {
        textContent: 'Settings'
      }))

      // Color scheme settings.
      settingsSection.appendChild(util.buildElement('label', {
        textContent: 'Color scheme'
      }, colorSchemeLabel => {
        colorSchemeLabel.addEventListener('change', event => {
          this.userSettings.colorScheme = event.target.value
          this.makeColors()
        })

        const colorSchemeRadio = util.makeRadio('color-scheme', {
          auto: 'Automatic',
          light: 'Light',
          dark: 'Dark'
        })
        colorSchemeRadio.className = 'small-radio'
        this.elements.colorSchemeRadio = colorSchemeRadio
        colorSchemeLabel.appendChild(colorSchemeRadio)
      }))

      // Grid size settings
      settingsSection.appendChild(util.buildElement('label', {
        textContent: 'Grid size'
      }, gridSizeLabel => {
        gridSizeLabel.appendChild(util.buildElement('div', {}, gridSizeInputs => {
          // Grid size varies between 1 and 10. Larger is uninteresting,
          // smaller is literally impossible. :)

          // Width input.
          gridSizeInputs.appendChild(this.elements.widthInput = util.buildElement('input', {
            type: 'number',
            min: 1,
            max: 10,
            value: variables.boardDimensions[0]
          }, widthInput => {
            widthInput.addEventListener('change', () => {
              this.userSettings.boardWidth = parseInt(widthInput.value)
              this.reset()
            })
          }))

          gridSizeInputs.appendChild(document.createTextNode('x'))

          // Height input.
          gridSizeInputs.appendChild(this.elements.heightInput = util.buildElement('input', {
            type: 'number',
            min: 1,
            max: 10,
            value: variables.boardDimensions[1]
          }, heightInput => {
            heightInput.addEventListener('change', () => {
              this.userSettings.boardHeight = parseInt(heightInput.value)
              this.reset()
            })
          }))
        }))
      }))

      // Undo button settings
      settingsSection.appendChild(util.buildElement('label', {
        textContent: 'Show undo button'
      }, showUndoButtonLabel => {
        showUndoButtonLabel.addEventListener('change', event => {
          if (event.target.value === 'yes') {
            this.userSettings.showUndoButton = true
          } else {
            this.userSettings.showUndoButton = false
          }

          this.updateUndoButton()
        })

        const showUndoButtonRadio = util.makeRadio('show-undo-button', {
          yes: 'Yes',
          no: 'No'
        })
        showUndoButtonRadio.className = 'small-radio'
        this.elements.showUndoButtonRadio = showUndoButtonRadio
        showUndoButtonLabel.appendChild(showUndoButtonRadio)
      }))
    })

    this.elements.game.appendChild(this.elements.settingsSection)

    this.elements.game.appendChild(this.elements.gameContainer = util.buildElement('div', {
      id: 'gameContainer',
      tabIndex: 0 // In order to make the game window keyboard-focusable.
    }, gameContainer => {
      gameContainer.appendChild(this.canvas = util.buildElement('canvas', {
        // The size will be set by `this.resize()`.
        width: 0,
        height: 0
      }))

      gameContainer.appendChild(this.elements.undoButton = util.buildElement('button', {
        textContent: 'Undo',
        className: 'undo'
      }, undoButton => {
        undoButton.addEventListener('click', this.undoMove.bind(this))
      }))
    }))

    // Version information.
    this.elements.game.appendChild(util.buildElement('footer', {
      textContent: `2^11 - version ${settings.version}`
    }))
  }

  /// This function initializes every event listener the game uses,
  /// including keyboard and pointer input.
  listen () {
    this.elements.gameContainer.addEventListener('keydown', this.key.bind(this))

    // Keep track of whether the game is focused or not.
    this.focused = false
    this.elements.gameContainer.addEventListener('focus', () => {
      this.focused = true
      this.variables.needRedraw = true
    })
    this.elements.gameContainer.addEventListener('blur', () => {
      this.focused = false
      this.variables.needRedraw = true
    })

    // This variable holds the coordinates of every pointer
    // (i.e. finger, stylus, mouse) currently pressed on the game canvas.
    const pointers = {}

    this.canvas.addEventListener('pointerdown', event => {
      pointers[event.pointerId] = [event.clientX, event.clientY]
    })

    this.canvas.addEventListener('pointerup', event => {
      // Sometimes a pointer will be released without having an entry in
      // `pointers`, because it started pressing somewhere else on screen. We ignore it.
      if (pointers[event.pointerId] === undefined) return

      // Compute the pixels travelled between pointer down and up.
      const dx = pointers[event.pointerId][0] - event.clientX
      const dy = pointers[event.pointerId][1] - event.clientY

      const distance = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2))
      // We ignore any swipe that is less than 1 pixel in length.
      // They are usually clicks and taps without any moving, so they shoudln't
      // count as a swipe. Plus, a zero-length swipe doesn't have a direction,
      // so it would mess up the logic below.
      if (distance < 1) {
        return
      }

      // Register a move in either cardinal direction (up, down, left, right).
      const angle = Math.atan2(dy, dx)
      const direction = util.angleToDirection(angle)
      this.direction(direction)

      // Since the pointer has left the screen, we should
      // no longer keep track of it.
      delete pointers[event.pointerId]
    })

    // Whenever the game element resizes, we call `this.resize` to resize the
    // game canvas accordingly.
    const resizeObserver = new ResizeObserver(this.resize.bind(this))
    resizeObserver.observe(this.elements.game)

    // Keep track of whether the user prefers reduced motion.
    const reducedMotionQuery = window.matchMedia('(prefers-reduced-motion: reduce)')
    reducedMotionQuery.addEventListener('change', () => {
      this.reducedMotion = reducedMotionQuery.matches
    })
    this.reducedMotion = (!reducedMotionQuery || reducedMotionQuery.matches)

    // Keep track of whether the user prefers a specific color scheme.
    // This can be overrided by a user setting.
    const darkModeQuery = window.matchMedia('(prefers-color-scheme: dark)')
    this.darkMode = (!darkModeQuery || darkModeQuery.matches)
    darkModeQuery.addEventListener('change', () => {
      this.darkMode = darkModeQuery.matches

      this.makeColors()
    })

    // Keep track of whether the user prefers a higher constrast color scheme.
    const contrastQuery = window.matchMedia('(prefers-contrast: more)')
    this.moreContrast = (!contrastQuery || contrastQuery.matches)
    contrastQuery.addEventListener('change', () => {
      this.moreContrast = contrastQuery.matches

      this.makeColors()
    })

    // Save the game when the page's visibility state goes to 'hidden'.
    // This happens before the page is closed or unloaded, but it might not
    // always fire, especially on mobile devices. This is why we also save
    // periodically.
    document.addEventListener('visibilitychange', () => {
      if (document.visibilityState === 'hidden') {
        this.save()
      }
    })
  }

  /// Loads the game: starts saving periodically, adds the game element
  /// to the DOM, and starts drawing frames.
  load () {
    setInterval(this.save.bind(this), 1000)
    document.getElementsByTagName('main')[0].appendChild(this.elements.game)

    window.requestAnimationFrame(this.frame.bind(this))

    this.canvas.focus()
  }

  /// Starts a new game. This just resets the game state.
  newGame () {
    this.reset()
  }

  /// Make colors according to settings of color scheme and high contrast.
  makeColors () {
    document.documentElement.classList.remove('light')
    document.documentElement.classList.remove('dark')

    if (this.userSettings.colorScheme === 'auto') {
      colors.makeColors(this.darkMode, this.moreContrast)
    } else {
      const userDarkMode = this.userSettings.colorScheme === 'dark'
      colors.makeColors(userDarkMode, this.moreContrast)

      if (userDarkMode) {
        document.documentElement.classList.add('dark')
      } else {
        document.documentElement.classList.add('light')
      }
    }

    const userColorSchemeIndex = [
      'auto', 'light', 'dark'
    ].indexOf(this.userSettings.colorScheme)
    this.elements.colorSchemeRadio.querySelectorAll('input[type="radio"]')[userColorSchemeIndex].checked = true

    // Since the game colors might have changed, request a redraw.
    this.variables.needRedraw = true
  }

  updateUndoButton () {
    if (this.userSettings.showUndoButton) {
      this.elements.undoButton.classList.remove('hidden')
    } else {
      this.elements.undoButton.classList.add('hidden')
    }

    const showUndoButtonIndex = this.userSettings.showUndoButton ? 0 : 1
    this.elements.showUndoButtonRadio.querySelectorAll('input[type="radio"]')[showUndoButtonIndex].checked = true
  }

  /// Resize the game canvas according to the current board dimensions
  /// (number of columns and rows) and the game element size.
  resize () {
    const [columns, rows] = this.variables.boardDimensions

    const boundingRectangle = this.elements.game.getBoundingClientRect()
    this.variables.sizes.boardSize[0] = boundingRectangle.width

    // This is just a reworked equation starting from
    // [1] width = columns * tileSize + (columns + 1) * spacingLength
    // [2] spacingLength = tileSize * spacingProportion
    // From there it's just simple algebra to work out the
    // tile size and spacing length. I say simple algebra to try to soothe
    // the pain of having messed it up once before getting it right.

    this.variables.sizes.tileSize = this.variables.sizes.boardSize[0] / ((columns + 1) * settings.spacingProportion + columns)
    this.variables.sizes.spacingLength = this.variables.sizes.boardSize[0] / (columns / settings.spacingProportion + (columns + 1))

    this.variables.boardAspectRatio = (columns * this.variables.sizes.tileSize + (columns + 1) * this.variables.sizes.spacingLength) /
      (rows * this.variables.sizes.tileSize + (rows + 1) * this.variables.sizes.spacingLength)

    this.variables.sizes.boardSize[1] = boundingRectangle.width / this.variables.boardAspectRatio

    this.canvas.style.borderRadius = Math.floor(2 * settings.tileBorderRadius * this.variables.sizes.tileSize) + 'px'

    this.canvas.width = Math.floor(this.variables.sizes.boardSize[0])
    this.canvas.height = Math.floor(this.variables.sizes.boardSize[1])
    // We must regenerate the context every time the canvas is resized.
    this.context = this.canvas.getContext('2d')

    this.variables.needRedraw = true
  }

  /// Resets the board state by clearing the board and spawning two starting
  /// tiles. The running score is reset as well.
  reset () {
    this.variables.gameOver = false
    this.variables.board = []
    this.variables.tiles = []
    this.variables.history = []

    this.variables.boardDimensions = settings.defaultBoardDimensions
    if (this.userSettings.boardWidth !== undefined) {
      this.variables.boardDimensions[0] = this.userSettings.boardWidth
      this.elements.widthInput.value = this.userSettings.boardWidth
    }
    if (this.userSettings.boardHeight !== undefined) {
      this.variables.boardDimensions[1] = this.userSettings.boardHeight
      this.elements.heightInput.value = this.userSettings.boardHeight
    }

    this.variables.scores.current = 0

    const tileCount = this.variables.boardDimensions[0] * this.variables.boardDimensions[1]
    for (let y = 0; y < tileCount; y++) {
      this.variables.board.push(null)
    }

    // If the board dimensions changed, we should update the canvas size.
    this.resize()

    placeNewTile()
    placeNewTile()
  }

  /// Save the current game state and settings to `localStorage`.
  save () {
    const saveData = JSON.stringify({
      // Record only the tile value or `null`.
      board: this.variables.board.map(tile => tile?.value ?? null),
      history: this.variables.history,

      scores: this.variables.scores,
      userSettings: this.userSettings
    })

    window.localStorage.twotoeleven = saveData
  }

  /// Load the game state and settings from `localStorage`.
  loadSave () {
    if (!Object.hasOwnProperty.call(window.localStorage, 'twotoeleven')) return

    const saveData = JSON.parse(window.localStorage.twotoeleven)

    if (saveData.userSettings !== undefined) {
      this.userSettings = saveData.userSettings

      // If we load user settings, we have to reset board state in case
      // they have changed.
      this.reset()
    }

    if (saveData.board !== undefined) {
      this.populateBoard(saveData.board)
    }

    if (saveData.history !== undefined) {
      this.variables.history = saveData.history
    }

    if (saveData.scores !== undefined) {
      this.variables.scores = saveData.scores
    }
  }

  /// This is used to make the `variables.tiles` animation data and the
  /// `variables.board` board state. Useful when jumping to a new board state
  /// like when loading a save or undoing a move.
  populateBoard (boardState) {
    this.variables.board = boardState.map((value, index) => {
      if (value === null) return null

      return new Tile(index, value)
    })

    this.variables.tiles = this.variables.board.filter(tile => tile !== null)
    this.variables.gameOver = !canMove()
  }

  /// Move the board in direction `d`, which is a `util.Direction` enum:
  /// either up, down, left, or right.
  direction (d) {
    if (this.variables.gameOver) {
      return
    }

    const currentBoardState = this.variables.board.map(tile => tile?.value ?? null)
    const currentScore = this.variables.scores.current

    // Keep track of whether anything moves when going in this direction.
    let didAnything = false

    const [columnCount, columnSize] = sliceDimensions(d)
    for (let x = 0; x < columnCount; x++) {
      const indexColumn = []

      for (let y = 0; y < columnSize; y++) {
        indexColumn.push(transformCoordinates(x, y, d))
      }

      // We keep a running list of tiles that have already merged and so cannot merge again in the same movement
      const nonMergeableTiles = new Set()
      let didSomething

      // Continue moving tiles until nothing can move anymore
      do {
        didSomething = false

        for (let i = columnSize - 1; i > 0; i--) {
          const current = indexColumn[i - 1]
          const next = indexColumn[i]

          // This is an empty tile and so it cannot move.
          if (this.variables.board[current] === null) continue

          // There is an empty spot further down of this tile, so we can move it
          if (this.variables.board[next] === null) {
            this.variables.board[current].move(next)
            this.variables.board[next] = this.variables.board[current]
            this.variables.board[current] = null

            didSomething = true

            if (nonMergeableTiles.has(current)) {
              nonMergeableTiles.delete(current)
              nonMergeableTiles.add(next)
            }
          // If there is a corresponding mergeable tile further down, we can merge the two
          } else if (
            this.variables.board[current].value === this.variables.board[next].value &&
            !nonMergeableTiles.has(current) &&
            !nonMergeableTiles.has(next)
          ) {
            const value = this.variables.board[next].value
            this.variables.scores.current += Math.pow(2, value + 1)

            this.variables.board[current].removeAfterAnimation = true
            this.variables.board[next].removeAfterAnimation = true

            this.variables.board[current].move(next)
            this.variables.board[next] = variables.board[current]
            this.variables.board[current] = null

            newTile(next, value + 1)

            this.variables.board[next].popUp()

            didSomething = true

            nonMergeableTiles.add(next)
          }
        }

        if (didSomething) didAnything = true
      } while (didSomething)
    }

    // If anything moved, we should create a new tile in an empty spot.
    if (didAnything) {
      this.variables.history.push({
        board: currentBoardState,
        score: currentScore
      })

      placeNewTile()
    }

    if (!canMove()) {
      this.variables.gameOver = true
    }
  }

  /// Undoes the last move by restoring the board state to the
  /// `variables.history` array's last element. Does nothing if the history is
  /// empty.
  undoMove () {
    if (this.variables.history.length === 0) {
      return
    }
    const { board, score } = this.variables.history.pop()

    this.populateBoard(board)
    this.variables.scores.current = score
  }

  /// Called when a keyboard key is pressed.
  key (event) {
    let keyDirection

    switch (event.key) {
      case 'ArrowLeft':
        keyDirection = util.Directions.Left
        break
      case 'ArrowRight':
        keyDirection = util.Directions.Right
        break
      case 'ArrowUp':
        keyDirection = util.Directions.Up
        break
      case 'ArrowDown':
        keyDirection = util.Directions.Down
        break
      case 'z':
        this.undoMove()
        return
      // If the key is not any that we recognize, we should stop.
      default:
        return
    }

    // Prevent the page from scrolling when arrow keys are pressed.
    // Accessibility note: I believe it is okay to override keyboard behavior
    // here because it only happens when the game canvas is focused. To scroll
    // the page with the keyboard, keyboard users can simply move focus away
    // using Tab and Shift+Tab.
    event.preventDefault()

    this.direction(keyDirection)
  }

  /// Animates the game, then renders a single frame, if needed.
  /// Also keeps track of high score. It calls itself to be run next frame
  /// using `window.requestAnimationFrame()`.
  frame (now) {
    // Work out how long it has been since a frame has been rendered, in order
    // to animate at the right speed no matter the refresh rate.
    // `dt` is in seconds.
    let dt
    if (this.variables.lastFrameTime === null) {
      dt = 0
    } else {
      dt = now - this.variables.lastFrameTime
    }
    this.variables.lastFrameTime = now

    // Keep track of whether anything has changed on screen.
    let anythingChanged = false

    // Run one step of animation for every tile.
    for (const tile of variables.tiles) {
      if (tile.animate(dt, this.reducedMotion)) {
        anythingChanged = true
      }
    }

    // If a tile has finished doing its "disappear" animation,
    // we should remove it from the active tiles.
    for (const [index, tile] of this.variables.tiles.entries()) {
      if (tile.toBeRemoved) {
        this.variables.tiles[index] = null
        anythingChanged = true
      }
    }
    this.variables.tiles = this.variables.tiles.filter(
      tile => tile !== null
    )

    // Let's not waste resources drawing something if nothing has moved.
    // For an average player, this is probably going to be most frames, so
    // it's really worth doing.
    if (!anythingChanged && !variables.needRedraw) {
      window.requestAnimationFrame(this.frame.bind(this))
      return
    }

    // Draw the frame in `drawing.js`.
    drawing.drawFrame()

    if (this.variables.scores.current > this.variables.scores.best) {
      this.variables.scores.best = this.variables.scores.current
    }

    elements.scoreDisplay.textContent = this.variables.scores.current
    elements.bestScoreDisplay.textContent = this.variables.scores.best

    // Since we have redrawn the screen, mark it as such.
    this.variables.needRedraw = false
    window.requestAnimationFrame(this.frame.bind(this))
  }
}

/// This represents a tile on screen and its current animation state.
class Tile {
  constructor (index, value) {
    const position = indexToCoordinates(index)

    this.value = value

    this.animation = {
      running: false,

      from: { position },
      to: { position }
    }

    this.removeAfterAnimation = false
    this.toBeRemoved = false

    // A tile starts out by appearing on screen.
    this.appear()
  }

  /// Start a "move" animation, if the tile has somewhere to go.
  move (index) {
    this.animation.to = { position: indexToCoordinates(index) }

    if (
      this.animation.from.position[0] === this.animation.to.position[0] &&
        this.animation.from.position[1] === this.animation.to.position[1]
    ) {
      return
    }

    this.animation.running = true
    this.animation.duration = settings.animationSpeeds.move
    this.animation.progress = 0
    this.scaleFunc = settings.scalingFunctions.still
  }

  /// Start an "appearing" animation.
  appear () {
    this.animation.running = true
    this.animation.duration = settings.animationSpeeds.appear
    this.scaleFunc = settings.scalingFunctions.appear
    this.animation.progress = 0
  }

  /// Start a "pop up" animation, which happens when a tile appears from the
  /// merge of two previous tiles.
  popUp () {
    this.animation.running = true
    this.animation.duration = settings.animationSpeeds.popUp
    this.scaleFunc = settings.scalingFunctions.pop
    this.animation.progress = 0
  }

  /// Update the currently running animation by one step.
  /// Returns whether the tile has changed appearance or position.
  animate (dt, reducedMotion = false) {
    // If no animation is running, we skip the rest of the function.
    if (!this.animation.running) {
      // When disappearing, a tile should be removed when it has stopped
      // animating. This marks the tile for removal by the `Game` instance.
      if (this.removeAfterAnimation) {
        this.toBeRemoved = true

        return true
      }

      return false
    }

    // Update the animation progress, which goes linearly from 0 to 1
    // in `this.animation.duration` seconds.
    this.animation.progress += dt / this.animation.duration

    // If it is the end of the animation or if we should skip animations,
    // put the tile in its final state.
    if (this.animation.progress >= 1 || reducedMotion) {
      // Make sure the tile is in a static state.
      this.animation.running = false
      this.animation.progress = 0
      this.animation.from = this.animation.to
      this.scaleFunc = settings.scalingFunctions.still

      if (this.removeAfterAnimation) this.toBeRemoved = true
    }

    return true
  }

  /// Draw the tile in its animated position.
  draw () {
    const movingProgress = settings.timingFunctions.moving(
      this.animation.progress
    )

    // We use smooth animation functions so that the animations don't start and
    // end abruptly.

    const scale = this.scaleFunc(this.animation.progress)

    const x = util.linearBezier(
      this.animation.from.position[0], this.animation.to.position[0]
    )(movingProgress)
    const y = util.linearBezier(
      this.animation.from.position[1], this.animation.to.position[1]
    )(movingProgress)

    drawing.drawTile(x, y, scale, this.value)
  }
}

/// Converts a board index from 0 to columns * rows, to (x,y) coordinates.
function indexToCoordinates (index) {
  return [
    index % variables.boardDimensions[0],
    Math.floor(index / variables.boardDimensions[0])
  ]
}

/// Convert absolute `x`,`y` coordinates into coordinates relative to a given
/// `orientation`. For example, in a 4x4 board (0, 1) rotated `Left` is (1, 3),
/// and rotated `Up` is (3, 2).
function transformCoordinates (x, y, orientation) {
  let newX, newY

  switch (orientation) {
    case util.Directions.Down:
      newX = x; newY = y
      break
    case util.Directions.Up:
      newX = variables.boardDimensions[0] - x - 1
      newY = variables.boardDimensions[1] - y - 1
      break
    case util.Directions.Left:
      newX = variables.boardDimensions[0] - y - 1
      newY = x
      break
    case util.Directions.Right:
      newX = y
      newY = variables.boardDimensions[1] - x - 1
      break
  }

  return newY * variables.boardDimensions[0] + newX
}

/// Places a new tile somewhere in a empty spot in the board.
/// If there are no free spots, does not do anything.
function placeNewTile () {
  const freeIndices = []

  for (let y = 0; y < variables.boardDimensions[1]; y++) {
    for (let x = 0; x < variables.boardDimensions[0]; x++) {
      const index = y * variables.boardDimensions[0] + x

      if (variables.board[index] === null) {
        freeIndices.push(index)
      }
    }
  }

  if (freeIndices.length !== 0) {
    // In the original game, a new tile has a 90% chance of being 2^1
    // and a 10% change of being 2^2.
    newTile(util.choose(freeIndices), Math.random() < 0.9 ? 1 : 2)
  }
}

/// Make a new tile at index `index` with value `value` and place it on the board.
function newTile (index, value) {
  const tile = new Tile(index, value)

  variables.tiles.push(tile)
  variables.board[index] = tile
}

/// Convert absolute dimensions to dimensions relative to a `direction`.
/// For example, a 7x2 board has dimensions 2x7 from the side.
function sliceDimensions (direction) {
  let columnCount, columnSize

  switch (direction) {
    case util.Directions.Up:
    case util.Directions.Down:
      columnCount = variables.boardDimensions[0]
      columnSize = variables.boardDimensions[1]
      break
    case util.Directions.Left:
    case util.Directions.Right:
      columnCount = variables.boardDimensions[1]
      columnSize = variables.boardDimensions[0]
      break
  }

  return [columnCount, columnSize]
}

/// Returns whether the user can still move.
function canMove () {
  // If there is at least one empty spot, we can definitely move.
  if (variables.board.includes(null)) {
    return true
  }

  // Otherwise, if we can merge tiles we can move as well.
  for (const direction of [
    util.Directions.Down,
    util.Directions.Right
  ]) {
    const [columnCount, columnSize] = sliceDimensions(direction)

    for (let x = 0; x < columnCount; x++) {
      for (let y = 0; y < columnSize - 1; y++) {
        const current = transformCoordinates(x, y, direction)
        const next = transformCoordinates(x, y + 1, direction)

        if (variables.board[current].value === variables.board[next].value) {
          return true
        }
      }
    }
  }

  // Otherwise, we cannot move. This means the end of the game.
  return false
}

// Create a new game and load it as soon as the DOM is loaded.
const game = new Game()
window.addEventListener('DOMContentLoaded', () => {
  game.load()
})
